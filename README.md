# **Проекты лабораторных работ**
---
+ Дисциплина: "Системы ввода/вывода"
+ Период: 2021
# **Выполнили**
---
+ Чэнь Шипин, гр.P3404C
+ У Хэнбо, гр.P3404C
# **Лабораторная работа 1**
---
+ Название: "Разработка драйверов символьных устройств"
+ Цель работы: Получить знания и навыки разработки драйверов символьных устройств для операционной системы Linux.
# **Задания по вариантам**
---
При записи в файл символьного устройства текста, содержащего цифры, должен запоминаться результат суммы всех чисел, разделенных другими символами (буквы, пробелы и т.п.). Последовательность полученных результатов с момента загрузки модуля ядра должна выводиться при чтении созданного файла /proc/varN в консоль пользователя.
При чтении из файла символьного устройства в кольцевой буфер ядра должен осуществляться вывод тех же данных, которые выводятся при чтении файла /proc/varN.
# **Инструкция по сборке**
---
Для сборки драйвера необходимо ввести команду make
# **Инструкция пользователя**
---
+ Для запуска драйвера необходимо ввести команду sudo insmod proc_example3.ko, которая загружает модуль ядра в систему .
+ Для просмотреть динамически сгенерированный номер устройства ввести команду dmesg, которая сгенерирует два номера устройства, старший номер устройства и младший номер устройства.
+ Для выгрузки модуля драйвера нужно ввести команду sudo rmmod proc_example3.ko.
```
[  313.932201] chr_init(): major = 243, minor = 0
```
+ Ввести команду sudo mknod / dev / var3 c 243 0, чтобы создать символьное устройство.
+ Для протестировать программу-скрипт ввести команду sudo bash ./test.sh, которая записывать данные на устройства / proc / var3 и / dev / var3
+ После запуска программа вычислит значение выражения. Вы можете запускать сценарий несколько раз, и модуль ядра будет записывать результат вычисления каждого выражения.
+ Для вывести тот же результат вычисления выражения ввести команду sudo cat / proc / var3 или sudo cat / dev / var3
```
yaoyao@mintv:~/Desktop/var3$ sudo cat /proc/var3
232
211
126
9628

```
+ Для удалить только что созданное символьное устройство ввести команду sudo rm / dev / var3

